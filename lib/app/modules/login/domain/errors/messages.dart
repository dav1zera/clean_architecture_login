class FailureMessages {
  static String get Offline_Connection => "Você está offline";
  static String get Failed_To_Recover_User_Logged =>
      "Error ao tentar recuperar informações do usuário";
  static String get Failed_To_Logout => "Error ao tentar fazer logout";
  static String get Invalid_Email => "Email inválido";
  static String get Invalid_Password => "Senha inválida";
  static String get Error_Login_User => "Erro ao logar usuário";
  static String get Error_Get_Logged_User =>
      "Erro ao pegar o usuário logado atual";
  static String get Error_Execute_Login => "Erro ao executar login";
}
